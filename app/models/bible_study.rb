# typed: strict
class BibleStudy < ApplicationRecord
  include BibleStudyAdmin
  enum status: { not_done: 0, in_progress: 1, done: 2 }
  belongs_to :confession, dependent: :destroy
end
