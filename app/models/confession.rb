# typed: true
class Confession < ApplicationRecord
  include ConfessionAdmin
  before_create :check_data_of_confession 

  has_one :bible_study, dependent: :destroy
  belongs_to :user
  #belongs_to :responsible, class_name: 'Admin', foreign_key: 'admin_id', inverse_of: :confession_responsibilities
  belongs_to :admin
  accepts_nested_attributes_for :bible_study, allow_destroy: true

  scope :recent, -> { order(created_at: :desc) }

  def check_data_of_confession 
    self.date_of_confession = DateTime.now if date_of_confession.nil?
  end 
end
