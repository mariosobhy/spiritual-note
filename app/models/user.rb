# typed: strict
# frozen_string_literal: true

class User < ActiveRecord::Base
  include UserAdmin
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  belongs_to :admin, inverse_of: :servants
  belongs_to :category, optional: true
  has_one :note, dependent: :destroy 
  has_many :confessions, dependent: :destroy

  def last_confession_date 
    if self.confessions.count > 0
      self.confessions.last&.created_at
    else
      self.created_at
    end
  end 
end
