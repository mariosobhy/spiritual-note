# typed: false
module UserAdmin
  extend ActiveSupport::Concern

  included do
    rails_admin do
      label "المعترف"
      label_plural "المعترفين"
      list do 
        include_fields :id, :name, :email, :phone_number, :address,
          :age, :notes, :date_of_birth,
          :job_or_university, :admin, :category
        field :name do 
          label "الأسم"
        end 
          
        field :email do 
          label "الأيميل"
        end 

        field :phone_number do 
          label "رقم التليفون"
        end 

        field :address do 
          label "العنوان" 
        end 

        field :age do 
          label "السن" 
        end 

        field :notes do 
          label "ملاحظات"
        end 

        field :date_of_birth do 
          label "تاريخ الميلاد"
          strftime_format "%Y-%m-%d"
        end 
        
        field :job_or_university do 
          label "الوظيفة أو الجامعة"
        end

        field :admin do 
          label "الأب الكاهن"
        end 

        field :category do 
          label "الفئة"
        end 
      end
      edit do 
        include_fields :id, :name, :email, :password, :password_confirmation,
          :phone_number, :address,
          :age, :notes, :date_of_birth, :date_of_birth,
          :job_or_university, :admin, :category

        field :name do 
          label "الأسم"
        end 
          
        field :email do 
          label "الأيميل"
        end 

        field :password do 
          label "كلمة السر"
        end 

        field :password_confirmation do 
          label "تأكيد كلمة السر"
        end 

        field :phone_number do 
          label "رقم التليفون"
        end 

        field :address do 
          label "العنوان" 
        end 

        field :age do 
          label "السن" 
        end 

        field :notes do 
          label "ملاحظات"
        end 

        field :date_of_birth do 
          label "تاريخ الميلاد"
          strftime_format "%Y-%m-%d"
        end 
        
        field :job_or_university do 
          label "الوظيفة أو الجامعة"
        end

        field :admin do 
          label "الأب الكاهن"
        end 

        field :category do 
          label "الفئة"
        end 
      end 
      show do 
        include_fields :id, :name, :email, :phone_number, :address,
          :age, :notes, :date_of_birth, :date_of_birth,
          :job_or_university, :admin, :category

        field :name do 
          label "الأسم"
        end 
          
        field :email do 
          label "الأيميل"
        end 

        field :phone_number do 
          label "رقم التليفون"
        end 

        field :address do 
          label "العنوان" 
        end 

        field :age do 
          label "السن" 
        end 

        field :notes do 
          label "ملاحظات"
        end 

        field :date_of_birth do 
          label "تاريخ الميلاد"
          strftime_format "%Y-%m-%d"
        end 
        
        field :job_or_university do 
          label "الوظيفة أو الجامعة"
        end

        field :admin do 
          label "الأب الكاهن"
        end 

        field :category do 
          label "الفئة"
        end 
      end 
    end 
  end
end
