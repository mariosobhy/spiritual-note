# typed: false
module ChurchAdmin
    extend ActiveSupport::Concern
  
    included do
      rails_admin do
        label "الكنيسة"
        label_plural "الكنائس"
        list do 
          include_fields :id, :name, :country, :city
          field :name do 
            label "الأسم"
          end 
            
          field :country do 
            label "الدولة"
          end 

          field :city do 
            label "المحافظة"
          end 
        end
        edit do 
            include_fields :id, :name, :country, :city
            field :name do 
              label "الأسم"
            end 
              
            field :country do 
              label "الدولة"
            end 
  
            field :city do 
              label "المحافظة"
            end 
        end 
        show do 
            include_fields :id, :name, :country, :city
            field :name do 
              label "الأسم"
            end 
              
            field :country do 
              label "الدولة"
            end 
  
            field :city do 
              label "المحافظة"
            end 
        end 
      end 
    end
  end
  