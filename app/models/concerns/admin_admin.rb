# typed: false
module AdminAdmin
  extend ActiveSupport::Concern

  included do
    rails_admin do
      label "الأب الكاهن"
      label_plural "الاًباء الكهنة"
      list do 
        include_fields :id, :name, :email, :church
        field :name do 
          label "الأسم"
        end 
          
        field :email do 
          label "الأيميل"
        end 
        
        field :church do 
          label "الكنيسة"
        end 
      end

      edit do 
        include_fields :id, :name, :email, :church, :password, :password_confirmation
        field :name do 
          label "الأسم"
        end 
          
        field :email do 
          label "الأيميل"
        end 

        field :password do 
          label "كلمة السر"
        end 

        field :password_confirmation do 
          label "تأكيد كلمة السر"
        end 
        
        field :church do 
          label "الكنيسة"
        end 
      end 

      show do 
        include_fields :id, :name, :email, :church
        field :name do 
          label "الأسم"
        end 
          
        field :email do 
          label "الأيميل"
        end 
        
        field :church do 
          label "الكنيسة"
        end 
      end 
    end 
  end
end
