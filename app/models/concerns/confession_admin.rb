# typed: false
module ConfessionAdmin
    extend ActiveSupport::Concern
  
    included do
      rails_admin do
        label "الأعتراف"
        label_plural "الأعترافات"
        list do 
          include_fields :id, :date_of_confession, :user, :admin
          field :date_of_confession do 
            label "تاريخ الأعتراف"
          end 
            
          field :user do 
            label "المعترف"
          end 

          field :admin do 
            label "الأب الكاهن"
          end 
        end
        edit do 
          include_fields :id, :date_of_confession, :user, :admin
          field :date_of_confession do 
            label "تاريخ الأعتراف"
          end 
            
          field :user do 
            label "المعترف"
          end 

          field :admin do 
            label "الأب الكاهن"
          end 
        end 
        show do 
          include_fields :id, :date_of_confession, :user, :admin
          field :date_of_confession do 
            label "تاريخ الأعتراف"
          end 
            
          field :user do 
            label "المعترف"
          end 

          field :admin do 
            label "الأب الكاهن"
          end 
        end 
      end 
    end
  end
  
