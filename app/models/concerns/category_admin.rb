# typed: false
module CategoryAdmin
    extend ActiveSupport::Concern
  
    included do
      rails_admin do
        label "الفئة"
        label_plural "الفئات"
        list do 
          include_fields :id, :name
          field :name do 
            label "الأسم"
          end 
        end
        edit do 
            include_fields :id, :name
            field :name do 
              label "الأسم"
            end 
        end 
        show do 
            include_fields :id, :name
            field :name do 
              label "الأسم"
            end 
        end 
      end 
    end
  end
  