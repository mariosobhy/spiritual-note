# typed: false
module BibleStudyAdmin
  extend ActiveSupport::Concern

  included do
    rails_admin do
      label "دراسة الكتاب"
      label_plural "دراسة الكتاب"
      list do
        include_fields :id, :name_of_book, :confession, :notes, :status
        field :name_of_book do
          label "أسم السفر"
        end

        field :user do
          label "المعترف"
          pretty_value do
            bindings[:object].confession.user.name
          end
        end

        field :admin do
          label "الأب الكاهن"
          pretty_value do
            bindings[:object].confession.admin.name
          end
        end

        field :notes do
          label "ملاحظات"
        end

        field :status do
          label "الحالة"
        end

        field :first_external_book do
          label "كتاب خارجي 1"
        end

        field :second_external_book do
          label "كتاب خارجي 2"
        end

        field :referent_name do
          label "أسم العظة"
        end

        field :referent_url do
          label "لينك العظة"
        end
      end
      edit do
        include_fields :id, :name_of_book, :confession, :notes, :status
        field :name_of_book do
          label "أسم السفر"
        end

        field :confession do
          label "رقم الأعتراف"
        end

        field :notes do
          label "ملاحظات"
        end

        field :status do
          label "الحالة"
        end

        field :first_external_book do
          label "كتاب خارجي 1"
        end

        field :second_external_book do
          label "كتاب خارجي 2"
        end

        field :referent_name do
          label "أسم العظة"
        end

        field :referent_url do
          label "لينك العظة"
        end
      end
      show do
        include_fields :id, :name_of_book, :confession, :notes, :status
        field :name_of_book do
          label "أسم السفر"
        end

        field :confession do
          label "رقم الأعتراف"
        end

        field :notes do
          label "ملاحظات"
        end

        field :status do
          label "الحالة"
        end

        field :first_external_book do
          label "كتاب خارجي 1"
        end

        field :second_external_book do
          label "كتاب خارجي 2"
        end

        field :referent_name do
          label "أسم العظة"
        end

        field :referent_url do
          label "لينك العظة"
        end
      end
    end
  end
end
