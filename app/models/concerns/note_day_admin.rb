# typed: false
module NoteDayAdmin
    extend ActiveSupport::Concern
    included do
      rails_admin do
        label "أيام النوتة الروحية"
        label_plural " أيام النوتة الروحية"
        list do 
          include_fields :id, :baker, :third, :sixth, :ninth, :elnoom, :nos_eleel, :salat_yasoo3, :created_at, :note
          field :baker do 
            label "صلاة باكر"
          end 
          field :third do 
            label "صلاة الساعة الثالثة"
          end 
          field :ninth do 
            label "صلاة الساعة التاسعة"
          end 
          field :sixth do 
            label "صلاة الساعة السادسة"
          end 
          field :elnoom do 
            label "صلاة النوم"
          end 
          field :nos_eleel do 
            label "صلاة نصف الليل"
          end 
          field :el8roob do 
            label "صلاة الغروب"
          end 
          field :salat_yasoo3 do 
            label "صلاة يسوع"
          end 
          field :created_at do 
            label "تاريخ الأنشاء"
            strftime_format "%Y-%m-%d"
          end 
          field :note do 
            label "رقم النوتة"
          end 
        end
        edit do 
          include_fields :id, :baker, :third, :sixth, :ninth, :elnoom, :nos_eleel, :salat_yasoo3, :created_at, :note
          field :baker do 
            label "صلاة باكر"
          end 
          field :third do 
            label "صلاة الساعة الثالثة"
          end 
          field :ninth do 
            label "صلاة الساعة التاسعة"
          end 
          field :sixth do 
            label "صلاة الساعة السادسة"
          end 
          field :elnoom do 
            label "صلاة النوم"
          end 
          field :nos_eleel do 
            label "صلاة نصف الليل"
          end 
          field :el8roob do 
            label "صلاة الغروب"
          end 
          field :salat_yasoo3 do 
            label "صلاة يسوع"
          end 
          field :created_at do 
            label "تاريخ الأنشاء"
            strftime_format "%Y-%m-%d"
          end 
          field :note do 
            label "رقم النوتة"
          end 
        end 
        show do 
          include_fields :id, :baker, :third, :sixth, :ninth, :elnoom, :nos_eleel, :salat_yasoo3, :created_at, :note
          field :baker do 
            label "صلاة باكر"
          end 
          field :third do 
            label "صلاة الساعة الثالثة"
          end 
          field :ninth do 
            label "صلاة الساعة التاسعة"
          end 
          field :sixth do 
            label "صلاة الساعة السادسة"
          end 
          field :elnoom do 
            label "صلاة النوم"
          end 
          field :nos_eleel do 
            label "صلاة نصف الليل"
          end 
          field :el8roob do 
            label "صلاة الغروب"
          end 
          field :salat_yasoo3 do 
            label "صلاة يسوع"
          end 
          field :created_at do 
            label "تاريخ الأنشاء"
            strftime_format "%Y-%m-%d"
          end 
          field :note do 
            label "رقم النوتة"
          end 
        end 
      end 
    end
  end
  
