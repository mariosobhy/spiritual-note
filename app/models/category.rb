# typed: strong
class Category < ApplicationRecord
  include CategoryAdmin
  has_many :users
end 
