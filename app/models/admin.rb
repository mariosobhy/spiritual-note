# typed: strict
# frozen_string_literal: true

class Admin < ActiveRecord::Base
  include AdminAdmin
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  has_many :servants, class_name: 'User', foreign_key: 'admin_id', inverse_of: :admin, dependent: :destroy
  #has_many :confession_responsibilities, class_name: 'Confession', foreign_key: 'admin_id', inverse_of: :responsible, dependent: :destroy  
  has_many :confessions, dependent: :destroy
  belongs_to :church
end
