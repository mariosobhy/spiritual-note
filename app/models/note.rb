# typed: strong
class Note < ApplicationRecord
  include NoteAdmin
  has_many :note_days, dependent: :destroy
  belongs_to :user
end
