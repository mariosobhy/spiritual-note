# typed: strong
class Church < ApplicationRecord
  include ChurchAdmin
  has_one :admin, dependent: :destroy
end
