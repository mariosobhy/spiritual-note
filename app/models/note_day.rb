# typed: strong
class NoteDay < ApplicationRecord
  include NoteDayAdmin
  belongs_to :note

  scope :recent, -> { order(created_at: :desc) }
end
