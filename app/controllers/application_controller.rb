# typed: false
class ApplicationController < ActionController::API
    include DeviseTokenAuth::Concerns::SetUserByToken
    
    devise_token_auth_group :member, contains: [:user, :admin]
    before_action :authenticate_member!, unless: :devise_controller?
end
