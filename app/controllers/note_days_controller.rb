# typed: false
class NoteDaysController < ApplicationController
  def index 
    load_note_days
    render_note_days 
  end

  def create 
    load_note
    build_note_day 
    save_note_day
  end 

  def destroy 
    load_note_day 
    @note_day.destroy 
    render_note_day
  end 

  private 
    def load_note_days 
      if current_member.is_a?(Admin) 
        @note_days = NoteDay.where(note_id: params[:note_id]).recent
      else 
        @note_days = current_member.note.note_days
      end 
    end

    def load_note 
      @note = current_member.note unless current_member.is_a?(Admin)
    end 

    def build_note_day 
      @note_day ||= @note.note_days.new 
      @note_day.attributes = note_day_params
    end 

    def save_note_day 
      if @note_day.save 
        render json: NoteDaySerializer.new(@note_day).serializable_hash
      else 
        render_note_day_errors
      end 
    end 
    
    def render_note_day 
      render json: NoteDaySerializer.new(@note_day).serializable_hash
    end 

    def render_note_day_errors 
      render json: { 
        error: @note_day.errors.full_messages.to_sentence
      }, status: :bad_request
    end 
    
    def note_day_params 
      params.require(:note_day).permit(:baker, :third, :sixth, :ninth, :elnoom, :nos_eleel, :el8roob, :salat_yasoo3, :note_id)
    end 
end
