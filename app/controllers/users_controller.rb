# typed: false
class UsersController < ApplicationController
  #ToDo we need to authenticate show and update actions 
  #If admin he/she have all permissions to do with user 
  #If current_user he/she have only permission with his/her user only
  # GET /users
  def index
    if current_member.is_a?(Admin) 
      render_users(current_member.servants)
    else 
      render_users(current_member) 
    end 
  end

  # GET /users/1
  def show
    load_user
    render_user
  end

  # POST /users
  def create
    build_user
    save_user 
  end

  # PATCH/PUT /users/1
  def update
   load_user 
   build_user 
   save_user
  end

  # DELETE /users/1
  def destroy
    load_user 
    @user.destroy 
    render json: UserSerializer.new(@user).serializable_hash
  end

  private
    def load_user 
      @user = User.find(params[:id]) 
    end 

    def build_user 
      @user ||= User.new 
      @user.attributes = user_params
    end 

    def save_user 
      if @user.save 
        render json: UserSerializer.new(@user).serializable_hash
      else 
        render_user_errors 
      end
    end 

    def render_user
      render json: UserSerializer.new(@user).serializable_hash
    end 

    def render_users(users)
      render json: UserSerializer.new(users).serializable_hash
    end 

    def render_user_errors 
      render json: {
        error: @user.errors.full_messages.to_sentence
      }, status: :bad_request
    end 

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :address, :date_of_birth, :job_or_university,
                                   :admin_id, :category_id, :age, :nickname, :notes, :phone_number)
    end
end
