# typed: false
class ConfessionsController < ApplicationController
  def index
    load_confessions
    render_confession
  end

  # GET /users/1
  def show
    load_confession
    render_confession
  end

  # POST /users
  def create
    build_confession
    save_confession
  end

  def update
    load_confession
    build_confession
    save_confession
  end

  # DELETE /users/1
  def destroy
    load_confession
    @confession.destroy
    render json: ConfessionSerializer.new(@confession).serializable_hash
  end

  private
    def load_confession
      @confession = Confession.find(params[:id])
    end

    def load_confessions
      if current_member.is_a?(Admin)
        @confession = Confession.where(user_id: params[:user_id]).recent
      else
        @confession = current_member.confessions.recent
      end

    end

    def build_confession
      @confession ||= Confession.new
      @confession.attributes = confession_params
    end

    def save_confession
      if @confession.save
        render json: ConfessionSerializer.new(@confession).serializable_hash
      else
        render_confession_errors
      end
    end

    def render_confession
      render json: ConfessionSerializer.new(@confession).serializable_hash
    end

    def render_confession_errors
      render json: {
        error: @confession.errors.full_messages.to_sentence
      }, status: :bad_request
    end

    # Only allow a trusted parameter "white list" through.
    def confession_params
      params.require(:confession).permit(:date_of_confession, :user_id, :admin_id, bible_study_attributes: [ :id, :name_of_book, :status, :first_external_book, :second_external_book, :referent_name, :referent_url, :notes, :confession_id])
    end
end
