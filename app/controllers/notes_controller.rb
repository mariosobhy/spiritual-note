# typed: false
class NotesController < ApplicationController
  def index 
    load_all_notes
    render_note
  end 

  def show 
    load_note 
    render_note
  end 

  def create 
    build_note 
    save_note
  end 

  def destroy 
    load_note 
    @note.destroy 
    render_note
  end 

  private 
    def load_note 
      @note = Note.find_by(user_id: params[:user_id])
    end 

    def load_all_notes 
      if current_member.is_a?(Admin)
        @note = Note.find_by(user_id: params[:user_id])
      else 
        @note = current_member.note
      end 
    end 
    
    def build_note 
      @note ||= Note.new 
      @note.attributes = note_params
    end 

    def save_note 
      if @note.save 
        render json: NoteSerializer.new(@note).serializable_hash
      else 
        render_note_errors
      end 
    end 
    
    def render_note 
      render json: NoteSerializer.new(@note).serializable_hash
    end 

    def render_note_errors 
      render json: { 
        error: @note.errors.full_messages.to_sentence
      }, status: :bad_request
    end 
    
    def note_params 
      params.require(:note).permit(:baker, :third, :sixth, :ninth, :elnoom, :nos_eleel, :el8roob, :salat_yasoo3, :user_id)
    end 
end
