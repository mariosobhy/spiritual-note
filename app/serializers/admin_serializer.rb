# typed: false
class AdminSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :email, :created_at, :updated_at 
  #belongs_to :church
  #has_many :confessions 
  #has_many :servants

  attribute :church do |object|
  {
    id: object.church.id,
    name: object.church.name, 
    country: object.church.country,
    city: object.church.city
  }
  end 

  attribute :confessions, if: Proc.new { |record| record.confessions.any? } do |object|
    object.confessions.map do |conf|
    {
      id: conf.id,
      date_of_confession: conf.created_at,
      servant: conf.user.name 
    }
    end 
  end 

  attribute :servants, if: Proc.new { |record| record.servants.any? } do |object|
    object.servants.map do |servant|
    { 
      id: servant.id,
      name: servant.name
    }
    end 
  end 
end
