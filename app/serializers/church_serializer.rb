# typed: false
class ChurchSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :country, :city 
  has_one :admin

  attribute :admin do |object|
  {
    id: object.admin.id, 
    name: object.admin.name
  }
  end 
end
