# typed: false
class BibleStudySerializer
  include FastJsonapi::ObjectSerializer
  attributes :name_of_book, :confession_id, :status, :first_external_book, :second_external_book, :referent_name, :referent_url, :created_at, :updated_at
  #belongs_to :confession

  attribute :confession do |object|
  {
    id: object.confession.id,
    date_of_confession: object.confession.created_at
  }
  end
end
