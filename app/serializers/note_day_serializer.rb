# typed: false
class NoteDaySerializer
  include FastJsonapi::ObjectSerializer
  attributes :baker, :third, :sixth, :ninth, :el8roob, :elnoom, :nos_eleel, :salat_yasoo3 
  belongs_to :note
end
