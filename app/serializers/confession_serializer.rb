# typed: false
class ConfessionSerializer
  include FastJsonapi::ObjectSerializer
  attributes :date_of_confession
  #has_one :bible_study
  #belongs_to :user
  #belongs_to :admin

  attribute :servant do |object|
    {
      id: object.user.id,
      name: object.user.name
    }
  end

  attribute :responsible do |object|
    {
      id: object.admin.id,
      name: object.admin.name
    }
  end

  attribute :bible_study do |object|
    {
      id: object.bible_study.id,
      name_of_book: object.bible_study.name_of_book,
      status: object.bible_study.status,
      notes: object.bible_study.notes,
      first_external_book: object.bible_study.first_external_book,
      second_external_book: object.bible_study.second_external_book,
      referent_name: object.bible_study.referent_name,
      referent_url: object.bible_study.referent_url,
    }
  end
end
