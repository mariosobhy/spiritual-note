# typed: false
class UserSerializer
  include FastJsonapi::ObjectSerializer
  #has_one :note 
  #has_many :confessions
  #belongs_to :admin 
  #belongs_to :category
  attributes :name, :email, :nickname, :phone_number, :address, :age, :date_of_birth, :job_or_university, :notes 

  attribute :admin do |object|
  {
    id: object.admin.id, 
    name: object.admin.name
  }
  end   

  attribute :last_confession_date

  attribute :category do |object|
  {
    id: object.category.id, 
    name: object.category.name
  }
  end 

  attribute :note, if: Proc.new { |record| record.note.present? } do |object|
  {
    id: object&.note&.id, 
    baker: object&.note&.baker,
    third: object&.note&.third,
    sixth: object&.note&.sixth,
    ninth: object&.note&.ninth,
    nos_eleel: object&.note&.nos_eleel,
    el8roob: object&.note&.el8roob,
    elnoom: object&.note&.elnoom
  }
  end 

  def include_last_confession_date? 
    object.confessions.any?
  end 
end
