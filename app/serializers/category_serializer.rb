# typed: false
class CategorySerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :created_at, :updated_at 
  #has_many :users

  attribute :servants, if: Proc.new { |record| record.users.any? } do |object|
    object.users.map do |user|
    {
      id: user.id, 
      name: user.name
    }
    end 
  end 
end
