# typed: false
class NoteSerializer
  include FastJsonapi::ObjectSerializer
  attributes :baker, :third, :sixth, :ninth, :el8roob, :elnoom, :nos_eleel, :salat_yasoo3 
  #has_many :note_days 
  #belongs_to :user

  attribute :user do |object| 
    {
      id: object.user.id,
      name: object.user.name
    }
  end 

  attribute :note_days, if: Proc.new { |record| record.note_days.any? } do |object|
    object.note_days.map do |note|
      {
        id: note.id, 
        baker: note.baker, 
        third: note.third, 
        sixth: note.sixth,
        ninth: note.ninth,
        el8roob: note.el8roob,
        elnoom: note.elnoom, 
        nos_eleel: note.nos_eleel, 
        created_at: note.created_at
      }
    end 
  end 
end
