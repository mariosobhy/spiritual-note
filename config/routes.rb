# typed: strict
Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount_devise_token_auth_for 'Admin', at: 'admin_auth'
  mount_devise_token_auth_for 'User', at: 'auth'

  resources :admins, except: %i[new edit]
  resources :users, except: %i[new edit] 
  resources :categories, except: %i[new edit]
  resources :confessions, except: %i[new edit] do 
    resources :bible_studies, except: %i[new edit]
  end 
  resources :notes, except: %i[new edit] do 
    resources :note_days, except: %i[new edit] 
  end 
end
