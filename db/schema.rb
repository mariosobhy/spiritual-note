# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_27_024722) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.bigint "church_id"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["church_id"], name: "index_admins_on_church_id"
    t.index ["confirmation_token"], name: "index_admins_on_confirmation_token", unique: true
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_admins_on_uid_and_provider", unique: true
  end

  create_table "bible_studies", force: :cascade do |t|
    t.string "name_of_book"
    t.bigint "confession_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "notes", default: ""
    t.integer "status", default: 0
    t.string "first_external_book", default: ""
    t.string "second_external_book", default: ""
    t.string "referent_name", default: ""
    t.string "referent_url", default: ""
    t.index ["confession_id"], name: "index_bible_studies_on_confession_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "churches", force: :cascade do |t|
    t.string "name"
    t.string "country"
    t.string "city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "confessions", force: :cascade do |t|
    t.datetime "date_of_confession"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "admin_id"
    t.index ["admin_id"], name: "index_confessions_on_admin_id"
    t.index ["user_id"], name: "index_confessions_on_user_id"
  end

  create_table "note_days", force: :cascade do |t|
    t.boolean "baker", default: false
    t.boolean "third", default: false
    t.boolean "sixth", default: false
    t.boolean "ninth", default: false
    t.boolean "el8roob", default: false
    t.boolean "elnoom", default: false
    t.boolean "nos_eleel", default: false
    t.bigint "note_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "salat_yasoo3", default: false
    t.index ["note_id"], name: "index_note_days_on_note_id"
  end

  create_table "notes", force: :cascade do |t|
    t.boolean "baker", default: false
    t.boolean "third", default: false
    t.boolean "sixth", default: false
    t.boolean "ninth", default: false
    t.boolean "el8roob", default: false
    t.boolean "elnoom", default: false
    t.boolean "nos_eleel", default: false
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "salat_yasoo3", default: false
    t.index ["user_id"], name: "index_notes_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.string "phone_number"
    t.string "address"
    t.integer "age"
    t.string "notes"
    t.date "date_of_birth"
    t.string "job_or_university"
    t.bigint "admin_id"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id"
    t.index ["admin_id"], name: "index_users_on_admin_id"
    t.index ["category_id"], name: "index_users_on_category_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  add_foreign_key "admins", "churches"
  add_foreign_key "bible_studies", "confessions"
  add_foreign_key "confessions", "users"
  add_foreign_key "note_days", "notes"
  add_foreign_key "notes", "users"
  add_foreign_key "users", "admins"
end
