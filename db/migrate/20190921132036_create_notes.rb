# typed: true
class CreateNotes < ActiveRecord::Migration[5.2]
  def change
    create_table :notes do |t|
      t.boolean :baker, default: false
      t.boolean :third, default: false
      t.boolean :sixth, default: false
      t.boolean :ninth, default: false
      t.boolean :el8roob, default: false
      t.boolean :elnoom, default: false
      t.boolean :nos_eleel, default: false
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
