class Add2ExternalBooksAnd3ezaToBibleStudy < ActiveRecord::Migration[5.2]
  def change
    add_column :bible_studies, :first_external_book, :string, default: ""
    add_column :bible_studies, :second_external_book, :string, default: ""
    add_column :bible_studies, :referent_name, :string, default: ""
    add_column :bible_studies, :referent_url, :string, default: ""
  end
end
