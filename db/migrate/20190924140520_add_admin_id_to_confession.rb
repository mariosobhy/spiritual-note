# typed: true
class AddAdminIdToConfession < ActiveRecord::Migration[5.2]
  def change
    add_reference :confessions, :admin, index: true
  end
end
