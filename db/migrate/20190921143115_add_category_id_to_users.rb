# typed: true
class AddCategoryIdToUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :category, index: true
  end
end
