# typed: true
class CreateBibleStudies < ActiveRecord::Migration[5.2]
  def change
    create_table :bible_studies do |t|
      t.string :name_of_book
      t.references :confession, foreign_key: true

      t.timestamps
    end
  end
end
