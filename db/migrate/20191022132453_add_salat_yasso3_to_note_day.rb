# typed: true
class AddSalatYasso3ToNoteDay < ActiveRecord::Migration[5.2]
  def change
    add_column :note_days, :salat_yasoo3, :boolean, default: false
  end
end
