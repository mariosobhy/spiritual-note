# typed: true
class RemoveDoneColumnFromBibleStudy < ActiveRecord::Migration[5.2]
  def change
    remove_column :bible_studies, :done, :boolean
  end
end
