# typed: true
class AddSalatYasso3ToNote < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :salat_yasoo3, :boolean, default: false
  end
end
