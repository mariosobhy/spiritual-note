# typed: true
class AddDoneToBibleStudy < ActiveRecord::Migration[5.2]
  def change
    add_column :bible_studies, :done, :boolean, default: false
    add_column :bible_studies, :notes, :string, default: ""
  end
end
