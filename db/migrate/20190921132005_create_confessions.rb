# typed: true
class CreateConfessions < ActiveRecord::Migration[5.2]
  def change
    create_table :confessions do |t|
      t.datetime :date_of_confession
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
