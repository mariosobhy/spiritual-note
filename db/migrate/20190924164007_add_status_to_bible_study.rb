# typed: true
class AddStatusToBibleStudy < ActiveRecord::Migration[5.2]
  def change
    add_column :bible_studies, :status, :integer, default: 0
  end
end
